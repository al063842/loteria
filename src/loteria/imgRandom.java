package loteria;

import java.awt.Color;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.IOException;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import static loteria.Sonido.ReproducirSonido;

public class imgRandom extends JFrame implements ActionListener {

    private JLabel label2; //declaramos nuestro JLabel
    JButton boton; //declaramos nuestro botón

    public imgRandom() throws IOException {
        getContentPane().setBackground(new Color(112, 198, 0));
        setVisible(true);

        setLayout(null);
        label2 = new JLabel(". . .");
        label2.setBounds(10, 20, 100, 100);
        add(label2);

        boton = new JButton("¡Saca una carta!");
        boton.setBounds(150, 250, 200, 100);
        add(boton);
        boton.addActionListener(this);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    ArrayList sonidos = new ArrayList();

    public void actionPerformed(ActionEvent e) {

        // un hilo
        Imagen hilo1 = new Imagen();
        this.add(hilo1);
        this.repaint();
        //Creacion del hilo
        Thread HiloImagen = new Thread(hilo1);
        //Llamada al hilo para ejecutar.
        HiloImagen.start();

        //sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\39monos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\1martillo.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\2palomas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\3piñas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\4brujas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\5jaula.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\6gallinas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\7barcos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\8flautas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\9pavos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\10serpientes.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\11damas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\12pescados.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\13conejos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\14gatos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\15iglesias.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\16venados.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\17sillas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\18banderas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\19palacios.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\20uvas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\21tren.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\22ahorcados.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\23melones.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\24adanyeva.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\25caballos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\26soldados.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\27soles.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\28mulas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\29fuertes.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\30almirez.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\31quinque.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\32nopal.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\33canasta.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\34aguilas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\35famas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\36cargador.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\37borrachos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\41mesedoras.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\42cometas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\43lunas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\44chues.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\45maromeros.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\38clarines.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\39monos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\40banderas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\46sandias.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\47negros.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\48tiradores.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\49sombreros.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\50mariposas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\51chivos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\52navajas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\53indios.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\54campanas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\55mangos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\56cachuchas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\57compases.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\58corazones.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\59sierras.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\60granadas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\61toros.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\62leones.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\63bailarinas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\64coronas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\65volcanes.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\66perros.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\67palmas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\68dados.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\69fresas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\70buhos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\71botas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\72arboles.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\73rosas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\74pericos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\75botellas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\76higos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\77guitarras.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\78manzanas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\79jarras.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\80liras.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\81vapor.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\82anclas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\83garzas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\84globos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\85liberatos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\86cipres.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\87casas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\88flamencos.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\89almohadillas.wav");
        sonidos.add("C:\\Users\\ameri\\Documents\\NetBeansProjects\\Loteria\\src\\loteria\\90mundos.wav");

        if (e.getSource() == boton) {
            ReproducirSonido((String) sonidos.get(hilo1.ra));
        }

    }

    public static void main(String ar[]) throws IOException {

        imgRandom form = new imgRandom();
        form.setBounds(0, 0, 500, 400);
        form.setVisible(true);
    }

}
